import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getWeChatUserList(params) {
  return request({
    url: api_prefix + 'wechat_user/list',
    method: 'get',
    params
  })
}

export function updateWeChatUser(data) {
  return request({
    url: api_prefix + 'wechat_user/update',
    method: 'put',
    data
  })
}
