<?php

return [
    // 别名或分组
    'alias' => [
        'check.business.operation' => \app\api\middleware\mp\v1\CheckOperationBusinessUser::class,
        'check.content.operation' => \app\api\middleware\mp\v1\CheckOperationContentUser::class,
    ],
];
