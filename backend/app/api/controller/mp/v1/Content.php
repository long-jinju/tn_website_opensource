<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-03
 * Time: 16:09
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\model\Content as ContentModel;

class Content extends BaseController
{

    /**
     * 获取对应栏目的分页内容数据
     * @http GET
     * @url /content/get_category_list
     * :type 栏目的类型
     * :category_id 栏目id
     * :page 页数
     * :limit 条目数
     * @return \think\response\Json
     */
    public function getCategoryPaginate()
    {
        $params = $this->request->get(['type','category_id','page','limit']);

        $data = ContentModel::getMpPaginationList($params);

        return tn_yes('获取对应栏目的分页内容数据成功', ['data' => $data]);
    }

    /**
     * 获得对应类型的推荐内容
     * @http GET
     * @url /content/get_recomm/:type/:limit
     * :type 需要获取推荐内容的类型
     * :limit 获取推荐内容的数目
     * @return \think\response\Json
     */
    public function getRecommData()
    {
        $params = $this->request->param(['type','limit'=>3]);

        $data = ContentModel::getRecommData($params);

        return tn_yes('获取推荐内容成功', ['data' => $data]);
    }

    /**
     * 获得对应类型的最新内容
     * @http GET
     * @url /content/get_newest/:type/:limit
     * :type 需要获取最新内容的类型
     * :limit 获取最新内容的数目
     * @return \think\response\Json
     */
    public function getNewestData()
    {
        $params = $this->request->param(['type','limit'=>3]);

        $data = ContentModel::getNewestData($params);

        return tn_yes('获取最新内容成功', ['data' => $data]);
    }

    /**
     * 根据对应的id获取详细内容信息
     * @http GET
     * @url /content/get_data/:type/:id
     * :type 获取内容的类型
     * :id 对应的内容id
     * @return \think\response\Json
     */
    public function getContentData()
    {
        $params = $this->request->param(['type','id']);

        $data = ContentModel::getMpShowContentByID($params);

        return tn_yes('获取内容详细信息成功', ['data' => $data]);
    }

    /**
     * 更新用户的操作信息
     * @http post
     * @url /content/update_operation
     * :id 对应的内容id
     * :type 对应需要操作的类型
     * :add_user 是否需要写入对应的用户信息
     * :only_check 是否只是检查用户是否存在
     * @return \think\response\Json
     */
    public function updateOperationUser()
    {
        $this->checkPostUrl();

        $params = $this->request->param(['id','type','add_user' => false, 'only_check' => false, 'get_user_list' => false]);

        $result = ContentModel::updateOperationUser($params);

        return tn_yes('更新用户操作信息成功', ['data' => $result]);
    }
}