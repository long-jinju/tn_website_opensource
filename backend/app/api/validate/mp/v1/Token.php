<?php


namespace app\api\validate\mp\v1;


use app\common\validate\BaseValidate;

class Token extends BaseValidate
{
    protected $rule = [
        'code' => 'require',
        'token' => 'require',
    ];

    protected $message = [
        'code.require' => 'code不能为空',
        'token.require' => 'token不能为空'
    ];

    protected $scene = [
        'get' => ['code'],
        'verify' => ['token'],
    ];
}