<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-18
 * Time: 11:18
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\AtlasCategory as AtlasCategoryModel;

class AtlasCategory extends BaseController
{
    /**
     * 获取图集类目分类的分页数据
     * @http get
     * @url /atlas_category/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = AtlasCategoryModel::getPaginationList($params);

        return tn_yes('获取图集类目分类信息成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取图集类目分类的数据
     * @http get
     * @url /atlas_category/get_by_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id', 0);

        $data = AtlasCategoryModel::getDataWithID($id);

        return tn_yes('获取图集类目分类信息成功', ['data' => $data]);
    }

    /**
     * 获取图集类目分类的全部名称信息
     * @http get
     * @url /atlas_category/get_all_name
     * @return \think\response\Json
     */
    public function getAllName()
    {
        $data = AtlasCategoryModel::getAllCategoryName();

        return tn_yes('获取图集类目分类的全部名称成功',['data' => $data]);
    }

    /**
     * 添加图集类目分类信息
     * @http post
     * @url /atlas_category/add
     * @return \think\response\Json
     */
    public function addAtlasCategory()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['name','sort','status']);

        $result = AtlasCategoryModel::addAtlasCategory($data);

        if ($result) {
            $this->request->log_content = '添加图集类目分类信息成功';
            return tn_yes('添加图集类目分类信息成功');
        } else {
            $this->request->log_content = '添加图集类目分类信息失败';
            return tn_no('添加图集类目分类信息失败');
        }
    }

    /**
     * 编辑图集类目分类信息
     * @http put
     * @url /atlas_category/edit
     * @return \think\response\Json
     */
    public function editAtlasCategory()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','name','sort','status']);

        $result = AtlasCategoryModel::editAtlasCategory($data);

        if ($result) {
            $this->request->log_content = '编辑图集类目分类信息成功';
            return tn_yes('编辑图集类目分类信息成功');
        } else {
            $this->request->log_content = '编辑图集类目分类信息失败';
            return tn_no('编辑图集类目分类信息失败');
        }
    }

    /**
     * 更新图集类目分类信息
     * @http put
     * @url /atlas_category/update
     * @return \think\response\Json
     */
    public function updateAtlasCategory()
    {
        $this->checkPutUrl();

        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = AtlasCategoryModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新图集类目分类信息成功';
            return tn_yes('更新图集类目分类信息成功');
        }else {
            $this->request->log_content = '更新图集类目分类信息失败';
            return tn_no('更新图集类目分类信息失败');
        }
    }

    /**
     * 删除图集类目分类信息
     * @http delete
     * @url /atlas_category/delete
     * @return \think\response\Json
     */
    public function deleteAtlasCategory()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = AtlasCategoryModel::deleteAtlasCategory($data);

        if ($result) {
            $this->request->log_content = '删除图集类目分类信息成功';
            return tn_yes('删除图集类目分类信息成功');
        }else {
            $this->request->log_content = '删除图集类目分类信息失败';
            return tn_no('删除图集类目分类信息失败');
        }
    }
}