<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-03
 * Time: 18:20
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\AuthMenu as AuthMenuModel;

class AuthMenu extends BaseController
{
    /**
     * 获取表格树形数据
     * @http get
     * @url /auth_menu/table_tree
     * @return \think\response\Json
     */
    public function getTableTree()
    {
        $params = $this->request->get();

        $data = AuthMenuModel::getTableTreeData($params);

        return tn_yes('获取TableTree数据成功',['data' => $data]);
    }

    /**
     * 获取树形结构数据
     * @http get
     * @url /auth_menu/element_tree
     * @return \think\response\Json
     */
    public function getElementTree()
    {
        $data = AuthMenuModel::getElementTreeData();

        return tn_yes('获取elementTree数据成功',['data' => $data]);
    }

    /**
     * 获取角色权限菜单的所有节点信息
     * @http get
     * @url /auth_menu/all_menu
     * @return \think\response\Json
     */
    public function getAllMenuNode()
    {
        $data = AuthMenuModel::getAllMenuNode();

        return tn_yes('获取所有节点信息成功',['data' => $data]);
    }

    /**
     * 获取指定父节点下的子节点数量
     * @http get
     * @url /auth_menu/get_children_count
     * @return \think\response\Json
     */
    public function getChildrenCount()
    {
        $pid = $this->request->get('pid',0);

        $count = AuthMenuModel::getChildrenCount($pid);

        return tn_yes('获取当前父节点的子节点数量成功',['count' => $count]);
    }

    /**
     * 根据id获取角色权限菜单的信息
     * @http get
     * @url /auth_menu/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id');

        $data = AuthMenuModel::getDataWithID($id);

        return tn_yes('获取角色权限菜单信息成功',['data' => $data]);
    }

    /**
     * 添加角色权限菜单
     * @http post
     * @url /auth_menu/add
     * @return \think\response\Json
     */
    public function addMenu()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['pid','name','title','path','url',
            'redirect','icon','is_link','hidden','is_click_in_breadcrumb',
            'always_show','no_cache','breadcrumb_show','public_menu','sort','status']);

        $result = AuthMenuModel::addAuthMenu($data);

        if ($result) {
            $this->request->log_content = '添加角色权限菜单成功';
            return tn_yes('添加角色权限菜单成功');
        }else {
            $this->request->log_content = '添加角色权限菜单失败';
            return tn_no('添加角色权限菜单失败');
        }
    }

    /**
     * 编辑角色权限菜单
     * @http post
     * @url /auth_menu/edit
     * @return \think\response\Json
     */
    public function editMenu()
    {
        $this->checkPutUrl();

        $data = $this->request->post(['id','pid','name','title','path','url',
            'redirect','icon','is_link','hidden','is_click_in_breadcrumb',
            'always_show','no_cache','breadcrumb_show','public_menu','sort','status']);

        $result = AuthMenuModel::editAuthMenu($data);

        if ($result) {
            $this->request->log_content = '编辑角色权限菜单成功';
            return tn_yes('编辑角色权限菜单成功');
        }else {
            $this->request->log_content = '编辑角色权限菜单失败';
            return tn_no('编辑角色权限菜单失败');
        }
    }

    /**
     * 更新角色权限菜单信息
     * @http put
     * @url /auth_menu/update
     * @return \think\response\Json
     */
    public function updateMenu()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = AuthMenuModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新角色权限菜单信息成功';
            return tn_yes('更新角色权限菜单信息成功');
        }else {
            $this->request->log_content = '更新角色权限菜单信息失败';
            return tn_no('更新角色权限菜单信息失败');
        }
    }

    /**
     * 删除角色权限菜单信息
     * @http delete
     * @url /auth_menu/delete
     * @return \think\response\Json
     */
    public function deleteMenu()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = AuthMenuModel::delAuthMenu($ids);

        if ($result) {
            $this->request->log_content = '删除角色权限菜单信息成功';
            return tn_yes('删除角色权限菜单信息成功');
        }else {
            $this->request->log_content = '删除角色权限菜单信息失败';
            return tn_no('删除角色权限菜单信息失败');
        }
    }
}