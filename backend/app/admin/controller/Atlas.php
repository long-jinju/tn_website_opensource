<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-22
 * Time: 10:59
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\Atlas as AtlasModel;

class Atlas extends BaseController
{
    /**
     * 修改图集中图片的所属栏目
     * @http put
     * @url /atlas/change_category
     * @return \think\response\Json
     */
    public function changeCategory()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','category_id']);

        $result = AtlasModel::changeCategory($data);

        return $result ? tn_yes('修改图片的所属栏目成功') : tn_no('修改图片所属栏目失败');
    }
}