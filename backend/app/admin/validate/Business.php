<?php


namespace app\admin\validate;


use app\common\validate\BaseValidate;

class Business extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'title' => 'require|max:60',
        'query_count' => 'number',
        'sort' => 'require|number|gt:0',
        'status' => 'require|number|between:0,1',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'title.require' => '业务标题不能为空',
        'title.max' => '业务标题最大长度为60',
        'query_count.number' => '查询次数格式不正确',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序格式不正确',
        'sort.gt' => '排序格式不正确',
        'status.require' => '状态不能为空',
        'status.number' => '状态格式不正确',
        'status.between' => '状态格式不正确',
    ];

    protected $scene = [
        'add' => ['title','query_count','sort','status'],
        'edit' => ['id','title','query_count','sort','status'],
    ];
}