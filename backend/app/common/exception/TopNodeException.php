<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-12
 * Time: 21:54
 */

namespace app\common\exception;


class TopNodeException extends BaseException
{
    public $code = 403;
    public $msg = '操作的元素为顶级并且有下级数据存在';
    public $errorCode = 10005;
}