<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-01-13
 * Time: 11:44
 */

namespace app\common\exception;


class WxPayException extends BaseException
{
    public $code = 401;
    public $msg = '拉起微信支付失败';
    public $errorCode = 50101;
}