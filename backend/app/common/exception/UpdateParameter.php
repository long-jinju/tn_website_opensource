<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-16
 * Time: 14:35
 */

namespace app\common\exception;


use app\common\validate\BaseValidate;

class UpdateParameter extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'field' => 'require',
        'value' => 'require',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'field_name.require' => '字段值不能为空',
        'value.require' => '需要修改的值不能为空',
    ];
}