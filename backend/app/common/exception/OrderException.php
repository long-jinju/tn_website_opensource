<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-05
 * Time: 15:21
 */

namespace app\common\exception;


class OrderException extends BaseException
{
    public $code = 404;
    public $msg = '订单不存在';
    public $errorCode = 50000;
}