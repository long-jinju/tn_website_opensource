<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-26
 * Time: 21:30
 */

namespace app\common\model;


class BusinessAdvisoryUser extends BaseModel
{
    // 关闭自动写入update_time字段
    protected $updateTime = false;
    public function user()
    {
        return $this->belongsTo('WeChatUser','user_id','id')->bind(['nick_name','avatar_url']);
    }
}